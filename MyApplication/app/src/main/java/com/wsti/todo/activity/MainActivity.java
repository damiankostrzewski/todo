package com.wsti.todo.activity;

import android.app.Notification;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.wsti.todo.R;
import com.wsti.todo.persistence.Task;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static com.wsti.todo.activity.TaskNotifier.CHANNEL_ID;

public class MainActivity extends AppCompatActivity {

    public static final int ADD_NOTE_REQUEST = 1;
    public static final int EDIT_NOTE_REQUEST = 2;

    private TaskViewModel taskViewModel;
    private RecyclerView recyclerView;
    private TaskAdapter adapter;

    private NotificationManagerCompat notificationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeFloatingActionButtonListener();
        initializeTaskAdapter();
        initializeRecyclerView();
        initializeItemTouch();

        taskViewModel = ViewModelProviders.of(this).get(TaskViewModel.class);
        taskViewModel.findAll().observe(this, adapter::setTasks);

        notificationManager = NotificationManagerCompat.from(this);
    }

    private void initializeFloatingActionButtonListener() {
        final FloatingActionButton buttonAddTask = findViewById(R.id.button_add_task);
        buttonAddTask.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, AddEditTaskActivity.class);
            startActivityForResult(intent, ADD_NOTE_REQUEST);
        });
    }

    private void initializeTaskAdapter() {
        adapter = new TaskAdapter();
        adapter.setOnItemClickListener(task -> {
            final Intent intent = new Intent(MainActivity.this, AddEditTaskActivity.class);
            intent.putExtra(AddEditTaskActivity.EXTRA_ID, task.getId());
            intent.putExtra(AddEditTaskActivity.EXTRA_CREATED_AT, task.getCreatedAt().toString());
            intent.putExtra(AddEditTaskActivity.EXTRA_DONE, task.isDone());
            intent.putExtra(AddEditTaskActivity.EXTRA_TITLE, task.getTitle());
            intent.putExtra(AddEditTaskActivity.EXTRA_DESCRIPTION, task.getDescription());
            intent.putExtra(AddEditTaskActivity.EXTRA_PRIORITY, task.getPriority());
            if (task.getFinishAt() == null) {
                intent.putExtra(AddEditTaskActivity.EXTRA_FINISHED_AT, "");
            } else {
                intent.putExtra(AddEditTaskActivity.EXTRA_FINISHED_AT, task.getFinishAt().toString());
            }
            startActivityForResult(intent, EDIT_NOTE_REQUEST);
        });
    }

    private void initializeRecyclerView() {
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }

    private void initializeItemTouch() {
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                taskViewModel.delete(adapter.getTaskAt(viewHolder.getAdapterPosition()));
                Toast.makeText(MainActivity.this, "Task deleted", Toast.LENGTH_SHORT).show();
            }
        }).attachToRecyclerView(recyclerView);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sendNotification();
    }

    private void sendNotification() {
        final int taskCountToDo = adapter.getTaskCountToDo();

        if (taskCountToDo > 0) {
            final String title = "You have something planned!";
            final String message = "There is " + taskCountToDo +
                    ((taskCountToDo == 1) ? " task" : " tasks") + " to do!";

            final Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_todo)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                    .build();

            notificationManager.notify(1, notification);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if ((requestCode == ADD_NOTE_REQUEST || requestCode == EDIT_NOTE_REQUEST) && resultCode == RESULT_OK) {
            final String createdAt = data.getStringExtra(AddEditTaskActivity.EXTRA_CREATED_AT);
            final boolean done = data.getBooleanExtra(AddEditTaskActivity.EXTRA_DONE, false);
            final String title = data.getStringExtra(AddEditTaskActivity.EXTRA_TITLE);
            final String description = data.getStringExtra(AddEditTaskActivity.EXTRA_DESCRIPTION);
            final int priority = data.getIntExtra(AddEditTaskActivity.EXTRA_PRIORITY, 1);
            final String finishedAt = data.getStringExtra(AddEditTaskActivity.EXTRA_FINISHED_AT);

            final LocalDate date = (finishedAt.isEmpty()) ? null : LocalDate.parse(finishedAt);

            final Task task = new Task(LocalDateTime.parse(createdAt), title, description, priority, date, done);

            if (requestCode == ADD_NOTE_REQUEST) {
                taskViewModel.insert(task);
                Toast.makeText(this, "Task inserted", Toast.LENGTH_SHORT).show();
            } else {
                final int id = data.getIntExtra(AddEditTaskActivity.EXTRA_ID, -1);
                if (id == -1) {
                    Toast.makeText(this, "Task can't be updated", Toast.LENGTH_SHORT).show();
                    return;
                }
                task.setId(id);
                taskViewModel.update(task);
                Toast.makeText(this, "Task updated", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Task not saved", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        final MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sort_by_created_at:
                adapter.sortByCreatedAt();
                Toast.makeText(this, "Sorted tasks by created at", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.sort_by_title:
                adapter.sortByTitle();
                Toast.makeText(this, "Sorted tasks by title", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.sort_by_priority:
                adapter.sortByPriority();
                Toast.makeText(this, "Sorted tasks by priority", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.sort_by_finished_at:
                adapter.sortByFinishedAt();
                Toast.makeText(this, "Sorted tasks by finished at", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.sort_by_done:
                adapter.sortByDone();
                Toast.makeText(this, "Sorted tasks by done", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.export_to_file:
                final TaskWriter taskWriter = new TaskWriter();
                if (taskWriter.exportTasks("ToDo_Tasks.txt", adapter.getCopiedTasks())) {
                    Toast.makeText(this, "Tasks are exported", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Tasks are't exported", Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.delete_all_tasks:
                taskViewModel.deleteAll();
                Toast.makeText(this, "All tasks deleted", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
