package com.wsti.todo.persistence;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.wsti.todo.persistence.converter.LocalDateConverter;
import com.wsti.todo.persistence.converter.LocalDateTimeConverter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity(tableName = "Task")
@TypeConverters({LocalDateConverter.class, LocalDateTimeConverter.class})
public class Task {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private LocalDateTime createdAt;

    private String title;

    private String description;

    private int priority;

    private LocalDate finishAt;

    private boolean done;

    public Task(LocalDateTime createdAt, String title, String description, int priority, LocalDate finishAt, boolean done) {
        this.createdAt = createdAt;
        this.title = title;
        this.description = description;
        this.priority = priority;
        this.finishAt = finishAt;
        this.done = done;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getPriority() {
        return priority;
    }

    public LocalDate getFinishAt() {
        return finishAt;
    }

    public boolean isDone() {
        return done;
    }
}
