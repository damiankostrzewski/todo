package com.wsti.todo.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.wsti.todo.R;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Locale;

public class AddEditTaskActivity extends AppCompatActivity {

    public static final String EXTRA_CREATED_AT = "com.wsti.todo.activity.EXTRA_CREATED_AT";
    public static final String EXTRA_DONE = "com.wsti.todo.activity.EXTRA_DONE";
    public static final String EXTRA_ID = "com.wsti.todo.activity.EXTRA_ID";
    public static final String EXTRA_TITLE = "com.wsti.todo.activity.EXTRA_TITLE";
    public static final String EXTRA_DESCRIPTION = "com.wsti.todo.activity.EXTRA_DESCRIPTION";
    public static final String EXTRA_PRIORITY = "com.wsti.todo.activity.EXTRA_PRIORITY";
    public static final String EXTRA_FINISHED_AT = "com.wsti.todo.activity.EXTRA_FINISHED_AT";

    private EditText editTextCreatedAt;
    private CheckBox checkBoxDone;
    private EditText editTextTitle;
    private EditText editTextDescription;
    private NumberPicker numberPickerPriority;
    private EditText editTextFinishedAt;

    private Calendar calendar = Calendar.getInstance();
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);

        editTextCreatedAt = findViewById(R.id.edit_text_created_at);
        checkBoxDone = findViewById(R.id.check_box_done);
        editTextTitle = findViewById(R.id.edit_text_title);
        editTextDescription = findViewById(R.id.edit_text_description);
        numberPickerPriority = findViewById(R.id.number_picker_priority);
        editTextFinishedAt = findViewById(R.id.edit_text_finish_at);

        numberPickerPriority.setMinValue(1);
        numberPickerPriority.setMaxValue(10);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);

        final Intent intent = getIntent();
        if (intent.hasExtra(EXTRA_ID)) {
            setTitle("Edit task");
            editTextCreatedAt.setText(intent.getStringExtra(EXTRA_CREATED_AT));
            checkBoxDone.setChecked(intent.getBooleanExtra(EXTRA_DONE, false));
            editTextTitle.setText(intent.getStringExtra(EXTRA_TITLE));
            editTextDescription.setText(intent.getStringExtra(EXTRA_DESCRIPTION));
            numberPickerPriority.setValue(intent.getIntExtra(EXTRA_PRIORITY, 1));
            editTextFinishedAt.setText(intent.getStringExtra(EXTRA_FINISHED_AT));
        } else {
            setTitle("Add Task");
            editTextCreatedAt.setText(LocalDateTime.now().toString());
        }

        editTextFinishedAt.setOnClickListener(v ->
                new DatePickerDialog(AddEditTaskActivity.this,
                (view, year, month, dayOfMonth) -> {
                    calendar.set(Calendar.YEAR, year);
                    calendar.set(Calendar.MONTH, month);
                    calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    updateEditTextFinishedAt();
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show());
    }

    private void updateEditTextFinishedAt() {
        final LocalDate selectedDate = LocalDate.parse(dateFormat.format(calendar.getTime()));

        if (selectedDate.isBefore(LocalDate.now())) {
            Toast.makeText(this, "Task can't be planned in the past", Toast.LENGTH_SHORT).show();
        } else {
            editTextFinishedAt.setText(selectedDate.toString());
        }
    }

    private void saveTask() {
        final String createdAt = editTextCreatedAt.getText().toString();
        final boolean done = checkBoxDone.isChecked();
        final String title = editTextTitle.getText().toString();
        final String description = editTextDescription.getText().toString();
        final int priority = numberPickerPriority.getValue();
        final String finishedAt = editTextFinishedAt.getText().toString();

        if (title.trim().isEmpty() || description.trim().isEmpty()) {
            Toast.makeText(this, "Please insert a title and description", Toast.LENGTH_SHORT).show();
            return;
        }

        final Intent data = new Intent();
        data.putExtra(EXTRA_CREATED_AT, createdAt);
        data.putExtra(EXTRA_DONE, done);
        data.putExtra(EXTRA_TITLE, title);
        data.putExtra(EXTRA_DESCRIPTION, description);
        data.putExtra(EXTRA_PRIORITY, priority);
        data.putExtra(EXTRA_FINISHED_AT, finishedAt);

        int id = getIntent().getIntExtra(EXTRA_ID, -1);
        if (id != -1) {
            data.putExtra(EXTRA_ID, id);
        }

        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        final MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.add_task_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_task:
                saveTask();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
