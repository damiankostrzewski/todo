package com.wsti.todo.persistence.converter;

import android.arch.persistence.room.TypeConverter;

import java.time.LocalDate;

public class LocalDateConverter {

    @TypeConverter
    public LocalDate fromTimestamp(final String value) {
        return (value == null) ? null : LocalDate.parse(value);
    }

    @TypeConverter
    public String toTimestamp(final LocalDate date) {
        return (date == null) ? null : date.toString();
    }
}
