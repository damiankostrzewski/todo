package com.wsti.todo.activity;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.wsti.todo.R;
import com.wsti.todo.persistence.Task;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.TaskHolder> {

    private List<Task> tasks = new ArrayList<>(0);
    private OnItemClickListener listener;

    @NonNull
    @Override
    public TaskHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.task_item, parent, false);

        return new TaskHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TaskHolder holder, int position) {
        final Task currentTask = tasks.get(position);

        holder.checkBoxDone.setChecked(currentTask.isDone());
        holder.textViewTitle.setText(currentTask.getTitle());
        holder.textViewDescription.setText(currentTask.getDescription());
        holder.textViewPriority.setText(String.valueOf(currentTask.getPriority()));
    }

    @Override
    public int getItemCount() {
        return tasks.size();
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
        notifyDataSetChanged();
    }

    public List<Task> getCopiedTasks() {
        return new ArrayList<>(tasks);
    }

    public int getTaskCountToDo() {
        return tasks.stream()
                .filter(t -> !t.isDone())
                .filter(t -> t.getFinishAt() == null ||
                        (t.getFinishAt().isEqual(LocalDate.now()) || t.getFinishAt().isBefore(LocalDate.now())))
                .collect(Collectors.toList())
                .size();
    }

    public Task getTaskAt(int position) {
        return tasks.get(position);
    }

    public void sortByCreatedAt() {
        tasks.sort(Comparator.comparing(Task::getCreatedAt));
        notifyDataSetChanged();
    }

    public void sortByTitle() {
        tasks.sort(Comparator.comparing(Task::getTitle, String::compareToIgnoreCase));
        notifyDataSetChanged();
    }

    public void sortByPriority() {
        tasks.sort(Comparator.comparing(Task::getPriority).reversed());
        notifyDataSetChanged();
    }

    public void sortByFinishedAt() {
        tasks.sort(Comparator.comparing(Task::getFinishAt, Comparator.nullsLast(Comparator.naturalOrder())));
        notifyDataSetChanged();
    }

    public void sortByDone() {
        tasks.sort(Comparator.comparing(Task::isDone));
        notifyDataSetChanged();
    }

    class TaskHolder extends RecyclerView.ViewHolder {
        private CheckBox checkBoxDone;
        private TextView textViewTitle;
        private TextView textViewDescription;
        private TextView textViewPriority;

        public TaskHolder(View itemView) {
            super(itemView);
            checkBoxDone = itemView.findViewById(R.id.check_box_done);
            textViewTitle = itemView.findViewById(R.id.text_view_title);
            textViewDescription = itemView.findViewById(R.id.text_view_description);
            textViewPriority = itemView.findViewById(R.id.text_view_priority);

            itemView.setOnClickListener(v -> {
                int position = getAdapterPosition();

                if (listener != null && position != RecyclerView.NO_POSITION) {
                    listener.onItemClick(tasks.get(position));
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Task task);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
