package com.wsti.todo.persistence.converter;

import android.arch.persistence.room.TypeConverter;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class LocalDateTimeConverter {

    @TypeConverter
    public LocalDateTime fromTimestamp(final long value) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(value), ZoneOffset.UTC);
    }

    @TypeConverter
    public long toTimestamp(final LocalDateTime dateTime) {
        return dateTime.atZone(ZoneOffset.UTC).toInstant().toEpochMilli();
    }
}
