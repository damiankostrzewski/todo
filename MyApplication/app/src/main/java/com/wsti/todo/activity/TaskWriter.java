package com.wsti.todo.activity;

import android.os.Environment;

import com.wsti.todo.persistence.Task;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.List;

public class TaskWriter {

    public boolean exportTasks(String fileName, List<Task> tasks) {
        final String path = Environment.getExternalStorageDirectory().getPath();
        final String absolutePath = path + "/" + fileName;

        try (PrintWriter output = new PrintWriter(new FileWriter(absolutePath))) {
            for (Task task : tasks) {
                output.println(task.toString());
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
